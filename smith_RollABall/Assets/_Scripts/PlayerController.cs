﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    // Player movement and game conditions 
    public float speed;
    public float jumpPower;
    bool onGround;
    bool wallChecker;
    bool reverseChecker;
    public Text countText;
    public Text winText;
    public Text wallText;
    public Text gameOverText;
    public Text reverseText;
    public GameObject pickupPrefab;

    Rigidbody rb;
    private int count;

    float timePassed = 0f;
    float spawnRange = 9.0f;

    int totalPickups;

    // Start is called before the first frame update
    void Start()
    {
        // Initializing player
        rb = GetComponent<Rigidbody>();

        // Keeping track of the score and winning condition and opening texts 
        count = 0;
        reverseText.text = "Press 'Z' for a surprise!";
        winText.text = "";
        wallText.text = "";
        wallChecker = false;
        reverseChecker = false;

        // Spawn the pick ups to begin the game  
        SpawnRandomPickUps();

        // Keep track of all the pick ups in an array and its length 
        GameObject[] allPickups = GameObject.FindGameObjectsWithTag("Pick Up");
        totalPickups = allPickups.Length;

        SetCountText();
    }

    // Making player jump 

    void Update()
    {
        // Check if reversed
        ReverseMode();
        // If the number of pick ups exceeds 100, end the game
        if (totalPickups >= 100)
        {
            gameOverText.text = "That was terrible! Find a new game to play";
            Time.timeScale = 0;
        }
        else
        {
            gameOverText.text = "";
        }


        // For first few seconds of the game, let the player know not to touch a wall with flashing text
        timePassed += Time.deltaTime;
        // Conversion to an integer 
        int timeInt = (int)timePassed;
        if ((timeInt <= 6) && (timeInt % 2 == 0) && (!wallChecker))
        {
            wallText.text = "Don't touch a wall! Bad things will happen";
        }
        else
        {
            if (wallChecker)
            {
                wallText.text = "You didn't listen! Now you have more work to do";
            }
            else
            {
                wallText.text = "";
            }
        }

        if (!reverseChecker)
        {
            reverseText.text = "Press 'Z' for a surprise!";
        }
        else
        {
            reverseText.text = "Reverse Mode activated! Your horizontal and vertical controls have now been flipped";
        }
    }


    // Update is called once per frame
    void FixedUpdate()
    {
        // Getting player movement 
        // Normal if not in reverse mode
        if (!reverseChecker)
        { 
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);


        rb.AddForce(movement * speed);
        }
        else
        {
            float moveHorizontal = Input.GetAxis("Vertical");
            float moveVertical = Input.GetAxis("Horizontal");

            Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);


            rb.AddForce(movement * speed);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        // When player collides with pick up, adjusted score and game accordingly 
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
    }

    void SetCountText()
    {
        // Check if the score meets win coniditon 
        countText.text = "Count: " + count.ToString();
        if (count >= totalPickups)
        {
            winText.text = "You Win!";
        }
    } 
    // A helper function for spawning pick ups 
    void SpawnPickUp()
    {
        Vector3 randomPosition = new Vector3(Random.Range(-spawnRange, spawnRange), 0.5f, Random.Range(-spawnRange, spawnRange));
        Instantiate(pickupPrefab, randomPosition, pickupPrefab.transform.rotation);
    }
    // Spawning a new pick up at a random position 
    void SpawnRandomPickUps()
    {
        // New position for the pick up and spawn a random number 
        for (int i = 0; i < Random.Range(16, 20); i++) {
            SpawnPickUp();
        }
    }

    // If a player hits a wall, make double the number of pick ups  
    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Wall"))
        {
            for (int i = 0; i < totalPickups; i++) {
                SpawnPickUp();
               
            }
            totalPickups *= 2;
            wallChecker = true;
        }
    }

    // Switching player into Reverse Mode for reversed controls 
    void ReverseMode()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            reverseChecker = true;
        }
    }
}
